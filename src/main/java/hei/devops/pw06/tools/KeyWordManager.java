package hei.devops.pw06.tools;

import hei.devops.pw06.enumeration.Alphabet;

public class KeyWordManager {
	
	public static  Integer getKey(String keyWord,Integer step)  {
	
		Integer index = step % keyWord.length();
		
		Alphabet keyLetter = Alphabet.valueof(keyWord.charAt(index));
		
		if(keyLetter !=null){
			return keyLetter.getPosition()-1;
		}
		System.err.println("Key word "+keyWord+" is not valid, key value is 0");
		return 0;
	}

}
