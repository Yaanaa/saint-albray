package hei.devops.pw06;

import java.util.Scanner;

import hei.devops.pw06.enumeration.ActionType;
import hei.devops.pw06.enumeration.RequestType;
import hei.devops.pw06.exception.ExitProgramException;
import hei.devops.pw06.service.EncryptionService;

public class Application {

	private static Scanner scanner= new Scanner(System.in);

	private static EncryptionService encryptionService;

	public static void main(String[] args) {
		try {
			while(true) {
				manageRequest();
			}

		}catch(ExitProgramException e) {
			System.out.println("Good bye !!!");
		}
	}

	private static void manageRequest() throws ExitProgramException {
		RequestType request = getRequest();
		manageEncryption(request);
		manageMessage();
		System.out.println();
	}

	private static void manageEncryption(RequestType request) throws ExitProgramException {
		switch(request){
		case CSR:
			break;
		case EXIT:
			throw new ExitProgramException();
		case MRS:
			break;
		case NVJ:
			break;
		case VGN:
			break;
		default:
			break;
	
		}
		
	}

	private static void manageMessage(){
		ActionType action = getAction();
		String result;
		switch (action) {
		case DECRYPT:
			result = encryptionService.decryptMessage(getAnswer("Write your message to decrypt :"));
			break;
		case ENCRYPT:
			result = encryptionService.encryptMessage(getAnswer("Write your message to encryp :"));
			break;
		default :
			result = new String("Erreur !!!!");
			break;
		}
		System.out.println(result);
	}
	
	
	
	private static RequestType getRequest(){
		try {
			String request = getAnswer("Write your request : ");
			return RequestType.valueOf(request);
		}catch(IllegalArgumentException e) {
			System.out.println("Possible values are : ");
			for(RequestType requestType : RequestType.values()) {
				System.out.println("  "+requestType);
			}
			return getRequest();
		}
	}

	private static ActionType getAction(){
		try {
			String action = getAnswer("Write your action : ");
			return ActionType.valueOf(action);
		}catch(IllegalArgumentException e) {
			System.out.println("Possible values are : ");
			for(ActionType actionType : ActionType.values()) {
				System.out.println("  "+actionType);
			}
			return getAction();
		}
	}

	private static String getAnswer(String question) {
		System.out.println(question);
		String response = scanner.next();
		System.out.println();
		return response;
	}

}
