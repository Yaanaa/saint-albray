package hei.devops.pw06.enumeration;

public enum ActionType {
	ENCRYPT,
	DECRYPT
}
