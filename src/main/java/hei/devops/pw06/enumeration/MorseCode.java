package hei.devops.pw06.enumeration;

public enum MorseCode {
	
	    A(".-"),
	    B("-..."),
	    C("-.-."),
	    D("-.."),
	    E("."),
	    F("..-."),
	    G("--."),
	    H("...."),
	    I(".."),
	    J(".---"),
	    K("-.-"),
	    L(".-.."),
	    M("--"),
	    N("-."),
	    O("---"),
	    P(".--."),
	    Q("--.-"),
	    R(".-."),
	    S("..."),
	    T("-"),
	    U("..-"),
	    V("...-"),
	    W(".--"),
	    X("-..-"),
	    Y("-.--"),
	    Z("--..");
	
    private Character  character;
    private String code;
    
    private MorseCode( String code) {
        this.character = this.name().charAt(0);
        this.code = code;
    }
    
    public Character getCharacter() {
    	return this.character;
    }
    
    public String  getCode() {
    	return this.code;
    }
    
    public String toString(){
    	return this.name();
    }

    
}
