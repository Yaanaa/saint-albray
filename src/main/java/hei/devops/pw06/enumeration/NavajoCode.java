package hei.devops.pw06.enumeration;

public enum NavajoCode {

	A("Wol-la-chee"),
	B("Shush"),
	C("Moasi"),
	D("Be"),
	E("Dzeh"),
	F("Ma-e"),
	G("Klizzie"),
	H("Lin"),
	I("Tkin"),
	J("Tkele-cho-gi"),
	K("Klizzie-yazzi"),
	L("Dibeh-yazzi"),
	M("Na-as-tso-si"),
	N("Nesh-chee"),
	O("Ne-ash-jah"),
	P("Bi-sodih"),
	Q("Ca-yeilth"),
	R("Gah"),
	S("Dibeh"),
	T("Than-zie"),
	U("No-da-ih"),
	V("A-keh-di-glini"),
	W("Gloe-ih"),
	X("Al-an-as-dzoh"),
	Y("Tsah-as-zih"),
	Z("Besh-do-gliz");

	private Character  character;
    private String code;
    
    private NavajoCode(String code) {
        this.character = this.name().charAt(0);
        this.code = code;
    }
    
    public Character getCharacter() {
    	return this.character;
    }
    
    public String  getCode() {
    	return this.code;
    }
    
    public String toString(){
    	return this.name();
    }
}
