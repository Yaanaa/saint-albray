package hei.devops.pw06.enumeration;

public enum Alphabet {
	A,
	B,
	C,
	D,
	E,
	F,
	G,
	H,
	I,
	J,
	K,
	L,
	M,
	N,
	O,
	P,
	Q,
	R,
	S,
	T,
	U,
	V,
	W,
	X,
	Y,
	Z;
	
	 private Character  character;
	 private Integer position;
	
	private Alphabet() {
		this.character = this.name().charAt(0);
		this.position =  character-64;
	}
	
	public Character getCharacter() {
		return this.character;
	}
	
	public Integer getPosition() {
		return this.position;
	}
	
	public static Alphabet valueof(Character character){
		for(Alphabet letter : Alphabet.values()) {
			if(letter.getCharacter().equals(character)) {
				return letter;
			}
		}
		return null;
	}
	
	public static Alphabet valueof(String character){
		for(Alphabet letter : Alphabet.values()) {
			if(letter.getCharacter().toString().equals(character)) {
				return letter;
			}
		}
		return null;
	}
	
	public static Alphabet valueof(Integer position) {
		for(Alphabet letter : Alphabet.values()) {
			if(letter.getPosition().equals(position)) {
				return letter;
			}
		}
		return null;
	}
	
	public String toString(){
		return this.name();
	}
}
