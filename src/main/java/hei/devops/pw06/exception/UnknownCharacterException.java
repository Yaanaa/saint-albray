package hei.devops.pw06.exception;

public class UnknownCharacterException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5183919123718828215L;
	
	private String  character;
	
	public UnknownCharacterException(Character c) {
		this.character=c.toString();
	}
	
	public UnknownCharacterException(String  c) {
		this.character=c;
	}

	public String  getCharacter() {
		return character;
	}
}
