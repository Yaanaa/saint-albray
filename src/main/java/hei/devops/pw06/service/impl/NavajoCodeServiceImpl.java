package hei.devops.pw06.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.enumeration.NavajoCode;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.EncryptionService;

public class NavajoCodeServiceImpl extends AbstractEncryptionServiceImpl implements EncryptionService {

	private final Logger log =LoggerFactory.getLogger(NavajoCodeServiceImpl.class);

	public NavajoCodeServiceImpl() {
	}
	
	@Override
	public String encryptCharacter(Character character) throws UnknownCharacterException {
		log.debug("Encrypt Character with navajo code | Character : {}",character);
		for(NavajoCode code : NavajoCode.values()) {
			if(code.getCharacter().equals(character)) {
				return code.getCode();
			}
		}
		throw new UnknownCharacterException(character);

	}

	@Override
	public Character decryptCharacter(String code) throws UnknownCharacterException {
		log.debug("Decrypt Character with navajo code | code : {}",code);
		for(NavajoCode navajoCode : NavajoCode.values()) {
			if(navajoCode.getCode().equals(code)) {
				return navajoCode.getCharacter();
			}
		}
		throw new UnknownCharacterException(code);
	}

	@Override
	public String getUnencryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getUnencryptedWordSeparator() {
		return " ";
	}

	@Override
	public String getEncryptedCharacterSeparator() {
		return " ";
	}

	@Override
	public String getEncryptedWordSeparator() {
		return "BE-TKAH";
	}

}