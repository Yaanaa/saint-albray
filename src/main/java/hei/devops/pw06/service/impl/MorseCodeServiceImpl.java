package hei.devops.pw06.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.enumeration.MorseCode;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.EncryptionService;

public class MorseCodeServiceImpl extends AbstractEncryptionServiceImpl implements EncryptionService {

	private final Logger log =LoggerFactory.getLogger(MorseCodeServiceImpl.class);

	public MorseCodeServiceImpl() {
	}

	@Override
	public String encryptCharacter(Character character) throws UnknownCharacterException {
		log.debug("Encrypt Character with morse code | Character : {}",character);

		for(MorseCode morseCode : MorseCode.values()) {
			if(morseCode.getCharacter().equals(character)) {
				return morseCode.getCode();
			}
		}
		throw new UnknownCharacterException(character);
	}

	@Override
	public Character decryptCharacter(String code) throws UnknownCharacterException {
		log.debug("Decrypt Character with morse code | Code : {}",code);

		for(MorseCode morseCode : MorseCode.values()) {
			if(morseCode.getCode().equals(code)) {
				return morseCode.getCharacter();
			}
		}
		throw new UnknownCharacterException(code);

	}

	@Override
	public String getUnencryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getUnencryptedWordSeparator() {
		return " ";
	}

	@Override
	public String getEncryptedCharacterSeparator() {
		return "/";
	}

	@Override
	public String getEncryptedWordSeparator() {
		return " ";
	}

}