package hei.devops.pw06.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.enumeration.Alphabet;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.EncryptionService;

public class CaesarCipherServiceImpl extends AbstractEncryptionServiceImpl implements EncryptionService {

	private final Logger log =LoggerFactory.getLogger(AbstractEncryptionServiceImpl.class);

	private Integer key;

	public CaesarCipherServiceImpl(Integer key) {
		super();
		this.key = key;
	}

	@Override
	public String encryptCharacter(Character character) throws UnknownCharacterException {
		log.debug("Encrypt Character with caesar cipher | Character : {} ; Key : {}",character,key);
		Alphabet letter  = Alphabet.valueof(character);
		if(letter != null) {
			Integer position =letter.getPosition();
			Integer newPosition = position + key; 
			if(newPosition < 1) {
				newPosition = newPosition +26 ;
			}

			if(newPosition > 26) {
				newPosition = newPosition - 26 ;
			}
			return Alphabet.valueof(newPosition).getCharacter().toString();
		}
		throw new UnknownCharacterException(character);

	}

	@Override
	public Character decryptCharacter(String character) throws UnknownCharacterException {
		log.debug("Encrypt Character with caesar cipher | Character : {} ; Key : {}",character,key);
		Alphabet letter  = Alphabet.valueof(character);
		if(letter != null) {
			Integer position =letter.getPosition();
			Integer newPosition = position - key; 
			if(newPosition < 1) {
				newPosition = newPosition +26 ;
			}

			if(newPosition > 26) {
				newPosition = newPosition - 26 ;
			}
			return Alphabet.valueof(newPosition).getCharacter();
		}
		throw new UnknownCharacterException(character);
	}

	@Override
	public String getUnencryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getUnencryptedWordSeparator() {
		return " ";
	}

	@Override
	public String getEncryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getEncryptedWordSeparator() {
		return " ";
	}

}