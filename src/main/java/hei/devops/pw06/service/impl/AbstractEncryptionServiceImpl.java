package hei.devops.pw06.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.exception.UnknownCharacterException;

public abstract  class AbstractEncryptionServiceImpl {

	private final Logger log =LoggerFactory.getLogger(AbstractEncryptionServiceImpl.class);

	public Logger getLog() {
		return log;
	}

	public String encryptMessage(String unencryptedMessage) {
		getLog().info("Encrypt Message | unencryptedMessage : {}",unencryptedMessage);
		StringBuilder encryptedMessage = new StringBuilder();
		for(String unencryptedCharacter : unencryptedMessage.split(getUnencryptedCharacterSeparator())) {
			getLog().trace("Encrypt Message |  Character : {}",unencryptedCharacter);
			if(getUnencryptedWordSeparator().equals(unencryptedCharacter)) {
				encryptedMessage = encryptedMessage.append(getEncryptedWordSeparator());
			}else {
				try {
					encryptedMessage = encryptedMessage.append(this.encryptCharacter(unencryptedCharacter.charAt(0)));
				}catch(UnknownCharacterException e) {
					getLog().warn("Encrypt Message | Character {} is not reconized",e.getCharacter());
					encryptedMessage= encryptedMessage.append(unencryptedCharacter);
				}
			}
			encryptedMessage = encryptedMessage.append(getEncryptedCharacterSeparator());
		}
		
		getLog().debug("Encrypt Message | encryptedMessage {}",encryptedMessage.toString());
		return encryptedMessage.toString();
	}


	public String decryptMessage(String encryptedMessage) {
		getLog().info("Decrypt Message | encryptedMessage : {}",encryptedMessage);

		StringBuilder decryptedMessage = new StringBuilder();

		for(String encryptedCharacter : encryptedMessage.split(getEncryptedCharacterSeparator())) {
			getLog().trace("Decrypt Message | Character : {}",encryptedCharacter);
			if(getEncryptedWordSeparator().equals(encryptedCharacter)) {
				decryptedMessage = decryptedMessage.append(getUnencryptedWordSeparator());
			}else {
				try {
					decryptedMessage = decryptedMessage.append(this.decryptCharacter(encryptedCharacter));
				}catch(UnknownCharacterException e) {
					getLog().warn("Decrypt Message | Character {} is not reconized",e.getCharacter());
					decryptedMessage = decryptedMessage.append(encryptedCharacter);
				}
			}
			decryptedMessage = decryptedMessage.append(getUnencryptedCharacterSeparator());

			
		}

		getLog().debug("Decrypt Message | decryptedMessage {}",decryptedMessage.toString());
		return decryptedMessage.toString();
	}
	
	


	public abstract String encryptCharacter(Character character) throws UnknownCharacterException;

	public abstract Character decryptCharacter(String character) throws UnknownCharacterException;

	public abstract String getUnencryptedCharacterSeparator();

	public abstract String getUnencryptedWordSeparator();

	public abstract String getEncryptedCharacterSeparator();

	public abstract String getEncryptedWordSeparator();

}
