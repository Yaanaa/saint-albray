package hei.devops.pw06.service.impl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.enumeration.Alphabet;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.EncryptionService;
import hei.devops.pw06.tools.KeyWordManager;

public class VigenereCipherServiceImpl extends AbstractEncryptionServiceImpl implements EncryptionService {

	private final Logger log =LoggerFactory.getLogger(VigenereCipherServiceImpl.class);
	
	private String keyWord;
	private Integer step;
		
	public VigenereCipherServiceImpl(String keyWord) {
		super();
		this.keyWord = keyWord;
		this.step = 0;
	}

	@Override
	public String encryptCharacter(Character character) throws UnknownCharacterException {
		log.debug("Encrypt Character with Vigenere cipher | Character : {} ;  KeyWord : {}  ; Step : {}",character, keyWord, step);
		Alphabet letter = Alphabet.valueof(character);
		if(letter !=null) {
			Integer position = letter.getPosition();
			Integer newPosition = position + KeyWordManager.getKey(keyWord, step);
			step++;
			if(newPosition < 1) {
				newPosition = newPosition +26 ;
			}
			
			if(newPosition > 26) {
				newPosition = newPosition - 26 ;
			}
			return Alphabet.valueof(newPosition).getCharacter().toString();
		}
		throw new UnknownCharacterException(character);

	}

	@Override
	public Character decryptCharacter(String character) throws UnknownCharacterException {
		log.debug("Decrypt Character with Vigenere cipher | Character : {} ;  KeyWord : {}  ; Step : {}",character, keyWord, step);
		Alphabet letter = Alphabet.valueof(character);
		if(letter !=null) {
			Integer position = letter.getPosition();
			Integer newPosition = position - KeyWordManager.getKey(keyWord, step);
			step++;
			if(newPosition < 1) {
				newPosition = newPosition +26 ;
			}
			
			if(newPosition > 26) {
				newPosition = newPosition - 26 ;
			}
			return Alphabet.valueof(newPosition).getCharacter();
		}
		throw new UnknownCharacterException(character);
	}

	@Override
	public String getUnencryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getUnencryptedWordSeparator() {
		return " ";
	}

	@Override
	public String getEncryptedCharacterSeparator() {
		return "";
	}

	@Override
	public String getEncryptedWordSeparator() {
		return " ";
	}

}
