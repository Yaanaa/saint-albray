package hei.devops.pw06.service;

public interface EncryptionService {
	
	 public String encryptMessage(String unencryptedMessage);
	
	 public String decryptMessage(String encryptedMessage);
	
}
