package hei.devops.pw06.service;

import static org.junit.Assert.fail;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import hei.devops.pw06.enumeration.NavajoCode;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.impl.NavajoCodeServiceImpl;

public class NavajoCodeServiceTestCase {

	private 	NavajoCodeServiceImpl serviceEncryption;

	@Before
	public  void initEachTest() {
		serviceEncryption = new NavajoCodeServiceImpl();

	}

	@Test
	public void shouldReturnEncryptCharacter() throws UnknownCharacterException {
		//GIVEN
		NavajoCode navajoCode = NavajoCode.K;

		//WHEN
		String resultat = serviceEncryption.encryptCharacter(navajoCode.getCharacter());

		//WHEN
		Assertions.assertThat(resultat).isEqualTo(navajoCode.getCode());
	}

	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnEncryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		Character character = 'k';

		//WHEN
		serviceEncryption.encryptCharacter(character);

		//WHEN
		fail();
	}
	
	@Test
	public void  shouldReturnDecryptCharacter() throws UnknownCharacterException {
		//GIVEN
		NavajoCode navajoCode = NavajoCode.K;
		
		//WHEN
		Character resultat = serviceEncryption.decryptCharacter(navajoCode.getCode());
		
		//WHEN
		Assertions.assertThat(resultat).isEqualTo(navajoCode.getCharacter());
	}
	
	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnDecryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		String code ="8498";
		
		//WHEN
		 serviceEncryption.decryptCharacter(code);
		
		//WHEN
		 fail();
	}




}

