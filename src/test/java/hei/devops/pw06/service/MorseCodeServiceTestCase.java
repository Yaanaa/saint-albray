package hei.devops.pw06.service;

import static org.junit.Assert.fail;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import hei.devops.pw06.enumeration.MorseCode;
import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.impl.MorseCodeServiceImpl;

public class MorseCodeServiceTestCase {

	private MorseCodeServiceImpl serviceEncryption;
	
	@Before
	public void initEachTest() {
		serviceEncryption = new MorseCodeServiceImpl();
	}
	
	
	@Test
	public void shouldReturnEncryptCharacter() throws UnknownCharacterException {
		//GIVEN
		MorseCode morseCode = MorseCode.K;
		
		//WHEN
		String result = serviceEncryption.encryptCharacter(morseCode.getCharacter());
		
		//THEN
		Assertions.assertThat(result).isEqualTo(morseCode.getCode());
	}
	
	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnEncryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		Character character = 'k';
		
		//WHEN
		 serviceEncryption.encryptCharacter(character);
		
		//THEN
		fail();
	}
	

	@Test
	public void shouldReturnDecryptCharacter() throws UnknownCharacterException {
		//GIVEN
		MorseCode morseCode = MorseCode.K;
		
		//WHEN
		Character result = serviceEncryption.decryptCharacter(morseCode.getCode());
		
		//THEN
		Assertions.assertThat(result).isEqualTo(morseCode.getCharacter());
	}
	
	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnDecryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		String code = "1234";
		
		//WHEN
		 serviceEncryption.decryptCharacter(code);
		
		//THEN
		fail();
	}
}
