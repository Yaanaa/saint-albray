package hei.devops.pw06.service;

import org.assertj.core.api.Assertions;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.slf4j.LoggerFactory;

import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.impl.AbstractEncryptionServiceImpl;

@RunWith(MockitoJUnitRunner.class)
public class EncryptionServiceTestCase {

	@Mock
	private AbstractEncryptionServiceImpl genericEncryptionService;
	
	@Test
	public void shouldEncryptMessage() throws UnknownCharacterException {

		//GIVEN
		Mockito.when(genericEncryptionService.getUnencryptedCharacterSeparator()).thenReturn("");
		Mockito.when(genericEncryptionService.getEncryptedCharacterSeparator()).thenReturn("#");
		Mockito.when(genericEncryptionService.getUnencryptedWordSeparator()).thenReturn(" ");
		Mockito.when(genericEncryptionService.getEncryptedWordSeparator()).thenReturn(" ");
		Mockito.when(genericEncryptionService.getLog()).thenReturn(LoggerFactory.getLogger(AbstractEncryptionServiceImpl.class));
		Mockito.when(genericEncryptionService.encryptMessage(Mockito.anyString())).thenCallRealMethod();
		Mockito.when(genericEncryptionService.encryptCharacter(Mockito.anyChar())).thenReturn("$");
		Mockito.when(genericEncryptionService.encryptCharacter('?')).thenThrow(new UnknownCharacterException('?'));
	
		//WHEN
		String encryptedMessage = genericEncryptionService.encryptMessage("ab cd?");
		
		//THEN
		InOrder inOrder = Mockito.inOrder(genericEncryptionService);
		inOrder.verify(genericEncryptionService).encryptCharacter('a');
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).encryptCharacter('b');
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).getEncryptedWordSeparator();
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).encryptCharacter('c');
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).encryptCharacter('d');
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();		
		inOrder.verify(genericEncryptionService).getEncryptedCharacterSeparator();
		
		Assertions.assertThat(encryptedMessage).isNotBlank().isEqualTo("$#$# #$#$#?#");
		
	}
	
	@Test
	public void shouldDecryptMessage() throws UnknownCharacterException {

		//GIVEN
		Mockito.when(genericEncryptionService.getUnencryptedCharacterSeparator()).thenReturn("");
		Mockito.when(genericEncryptionService.getEncryptedCharacterSeparator()).thenReturn("#");
		Mockito.when(genericEncryptionService.getUnencryptedWordSeparator()).thenReturn(" ");
		Mockito.when(genericEncryptionService.getEncryptedWordSeparator()).thenReturn(" ");
		Mockito.when(genericEncryptionService.getLog()).thenReturn(LoggerFactory.getLogger(AbstractEncryptionServiceImpl.class));
		Mockito.when(genericEncryptionService.decryptMessage(Mockito.anyString())).thenCallRealMethod();
		Mockito.when(genericEncryptionService.decryptCharacter(Mockito.anyString())).thenReturn('$');
		Mockito.when(genericEncryptionService.decryptCharacter("?")).thenThrow(new UnknownCharacterException('?'));
		

		//WHEN
		String decryptedMessage = genericEncryptionService.decryptMessage("a#b# #c#d#?");
		
		//THEN
		InOrder inOrder = Mockito.inOrder(genericEncryptionService);
		inOrder.verify(genericEncryptionService).decryptCharacter("a");
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).decryptCharacter("b");
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).getUnencryptedWordSeparator();
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).decryptCharacter("c");
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).decryptCharacter("d");
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();
		inOrder.verify(genericEncryptionService).getUnencryptedCharacterSeparator();

		Assertions.assertThat(decryptedMessage).isNotBlank().isEqualTo("$$ $$?");

	}
	
}
