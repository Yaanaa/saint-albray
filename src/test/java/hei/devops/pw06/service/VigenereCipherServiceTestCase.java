package hei.devops.pw06.service;

import static org.junit.Assert.fail;

import org.assertj.core.api.Assertions;
import org.junit.Before;
import org.junit.Test;

import hei.devops.pw06.exception.UnknownCharacterException;
import hei.devops.pw06.service.impl.VigenereCipherServiceImpl;

public class VigenereCipherServiceTestCase {

	private 	VigenereCipherServiceImpl serviceEncryption;

	@Before
	public  void initEachTest() {
		serviceEncryption = new VigenereCipherServiceImpl("KBCD");

	}

	@Test
	public void shouldReturnEncryptCharacter() throws UnknownCharacterException {
		//GIVEN
		Character character = 'A';

		//WHEN
		String resultat = serviceEncryption.encryptCharacter(character);

		//WHEN
		Assertions.assertThat(resultat).isEqualTo("K");
	}

	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnEncryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		Character character = 'a';

		//WHEN
		serviceEncryption.encryptCharacter(character);

		//WHEN
		fail();
	}

	@Test
	public void  shouldReturnDecryptCharacter() throws UnknownCharacterException {
		//GIVEN
		String  character = "K";

		//WHEN
		Character resultat = serviceEncryption.decryptCharacter(character);

		//WHEN
		Assertions.assertThat(resultat).isEqualTo('A');
	}

	@Test(expected=UnknownCharacterException.class)
	public void shouldReturnDecryptCharacterButThrowException() throws UnknownCharacterException {
		//GIVEN
		String  character = "k";

		//WHEN
		serviceEncryption.decryptCharacter(character);

		//WHEN
		fail();
	}

}
